import math

import arcade


class Food(arcade.Sprite):
    WIDTH = 64
    HEIGHT = 64
    SMELL_SIZE = 255

    def __init__(self, pos, size):
        super().__init__(filename='../res/pellet.png')
        x, y = pos
        self.center_x = x
        self.center_y = y
        self.scale = size

    def get_smell(self, x, y):
        return self.SMELL_SIZE * (self.scale) / max(math.sqrt((self.center_x - x) ** 2 + (self.center_y - y) ** 2),0.01)

    def update(self):
        super(Food, self).update()
