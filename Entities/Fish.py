import math
import random

import numpy as np
import tensorflow.keras as keras
import arcade


class Fish(arcade.Sprite):
    WIDTH = 64
    HEIGHT = 64
    MAX_TURNANGLE = 30

    def __init__(self, pos: (float, float), angle, speed, color, scale, aquarium):
        super().__init__(filename='../res/fish.png')
        self.remember_amount = 25
        self.color = color
        x, y = pos
        self.speed = speed
        self.angle = angle
        self.center_x = x
        self.center_y = y
        self.scale = scale
        self.aquarium = aquarium
        self.brain = self.getModel()
        self.last_steps = []

    def update(self):
        super(Fish, self).update()

        self.velocity[0] = -math.cos(math.radians(self.angle)) * self.speed
        self.velocity[1] = -math.sin(math.radians(self.angle)) * self.speed
        left_smell = self.aquarium.get_smell_at(self.left, self.top)
        right_smell = self.aquarium.get_smell_at(self.right, self.top)

        smell = np.array([[left_smell, right_smell]])
        prediction = self.brain.predict(smell)
        if len(self.last_steps) >= self.remember_amount:
            self.last_steps.pop(0)
        self.last_steps.append(([smell, prediction]))
        self.turn(prediction[0][0])

    def reward(self):
        if self.last_steps:
            smells = []
            predictions = []
            for step in self.last_steps:
                smell, prediction = step
                smells.append(smell)
                predictions.append(prediction)
            self.brain.fit(x=smells, y=predictions)

    def turn(self, angle):
        self.angle += max(min(angle, self.MAX_TURNANGLE), -self.MAX_TURNANGLE)
        self.angle %= 360

    def getModel(self):
        keras.backend.clear_session()
        input = keras.layers.Input(shape=(2), batch_size=1, dtype=float)

        dense = keras.layers.Dense(15, activation="relu", bias_initializer="random_normal")(input)
        out = keras.layers.Dense(1)(dense)

        model = keras.models.Model(inputs=input, outputs=out)
        model.compile('Adam', loss=keras.losses.binary_crossentropy)
        return model
