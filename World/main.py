import tensorflow
import arcade

from Entities.Fish import Fish
from Entities.Food import Food
from World.Aquarium import Aquarium

WIDTH = 800
HEIGHT = 600

if __name__ == '__main__':
    aquarium = Aquarium(WIDTH, HEIGHT)
    arcade.run()