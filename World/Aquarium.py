import arcade
import random

from arcade import Sprite

from Entities.Food import Food
from Entities.Fish import Fish

steps = 10


class Aquarium(arcade.Window):
    MIN_FISH = 2
    MIN_PELLETS = 10

    def __init__(self, width, height):
        super().__init__(width=width, height=height, title='Aquarium', resizable=True)
        self.fish_count = 0
        self.food_count = 0
        self.fish = arcade.SpriteList()
        self.food = arcade.SpriteList()
        arcade.set_background_color(arcade.color.WATERSPOUT)

    def generate_pellet(self):
        size = random.randrange(1, 2)
        pellet = Food(self.generate_valid_position(size * Food.WIDTH, size * Food.HEIGHT), size/7)
        self.food.append(pellet)

    def generate_valid_position(self, width_offset=0, height_offset=0):
        x = random.randrange(width_offset, self.width - width_offset)
        y = random.randrange(height_offset, self.height - height_offset)
        return (x, y)

    def generate_fish(self):
        angle = random.randrange(0, 360)
        fish = Fish(self.generate_valid_position(Fish.WIDTH, Fish.HEIGHT), angle, 5, arcade.color.AMBER, 1, self)
        self.fish.append(fish)

    def initialize(self):
        for _ in range(self.MIN_FISH):
            self.fish_count += 1
            self.generate_fish()

        for _ in range(self.MIN_PELLETS):
            self.food_count +=1
            self.generate_pellet()

    def on_draw(self):
        arcade.start_render()
        #steps =5
        #for x in range(int(self.width / steps)):
         #   for y in range(int(self.height / steps)):
          #      smell = self.get_smell_at(x * steps, y * steps)
         #       col = (255,255,smell*255)
           #     arcade.draw_point(x * steps, y * steps, col, size=steps)
        self.fish.draw()
        self.food.draw()

    def on_update(self, delta_time: float):
        if self.fish_count < self.MIN_FISH:
            self.generate_fish()
            self.fish_count += 1
        if self.food_count < self.MIN_PELLETS:
            self.generate_pellet()
            self.food_count += 1
        self.fish.update()
        self.food.update()
        fish: Fish
        for fish in self.fish:
            collide = fish.collides_with_list(self.food)
            if collide:
                fish.reward()
            for collide in collide:
                self.food.remove(collide)
                self.food_count -= 1

        for fish in self.fish:
            if fish.right < 0:
                fish.right = self.width
            if fish.left > self.width:
                fish.left = 0
            if fish.bottom > self.height:
                fish.bottom = 0
            if fish.top < 0:
                fish.top = self.height


    def get_smell_at(self, x, y):
        smell = 0
        food_item: Food
        for food_item in self.food:
            smell += food_item.get_smell(x, y)
        return smell
